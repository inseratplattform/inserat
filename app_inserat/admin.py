# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from app_inserat.models import DispatchType, PaymentType, Inserat, Offer

admin.site.register(DispatchType)
admin.site.register(PaymentType)
admin.site.register(Inserat)
admin.site.register(Offer)