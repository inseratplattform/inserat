# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from .serializers import InseratSerializer, OfferSerializer
from .models import Inserat, Offer


class ViewInserat(viewsets.ModelViewSet):
    serializer_class = InseratSerializer
    queryset = Inserat.objects.all()


class ViewOffer(viewsets.ModelViewSet):
    serializer_class = OfferSerializer
    queryset = Offer.objects.all()