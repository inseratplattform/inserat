from rest_framework.routers import DefaultRouter

from .views import ViewInserat, ViewOffer

router = DefaultRouter()
router.register(r'inserats', ViewInserat, base_name='inserats')
router.register(r'offers', ViewOffer, base_name='offers')
urlpatterns = router.urls