from rest_framework import serializers

from app_article.serializers import ArticleSerializer
from app_user.serializers import VendorSerializer, CustomerSerializer
from .models import Inserat, PaymentType, DispatchType, Offer


class PaymentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentType
        fields = ('id', 'cash', 'bank_transfer',)


class DispatchTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DispatchType
        fields = ('id', 'delivery_type',)


class InseratSerializer(serializers.ModelSerializer):
    article = ArticleSerializer(many=False)
    vendor = VendorSerializer(many=False)
    dispatch_type = DispatchTypeSerializer(many=False)
    payment_type = PaymentTypeSerializer(many=False)

    class Meta:
        model = Inserat
        fields = ('id', 'article', 'quantity', 'vendor', 'start_price', 'buy_price', 'increase_price', 'current_price', 'dispatch_type', 'payment_type',)


class OfferSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer(many=False)

    class Meta:
        model = Offer
        fields = ('id', 'price', 'won', 'article', 'customer',)