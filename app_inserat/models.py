# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class DispatchType(models.Model):
    delivery_type = models.CharField(max_length=20)

    def __str__(self):
        return self.delivery_type


class PaymentType(models.Model):
    cash = models.BooleanField()
    bank_transfer = models.BooleanField()

    def __str__(self):
        return 'Cash: %s Bank: %s' % (self.cash, self.bank_transfer)


class Offer(models.Model):
    customer = models.ForeignKey('app_user.Customer', on_delete=models.DO_NOTHING)
    price = models.DecimalField(decimal_places=2, max_digits=6)
    article = models.ForeignKey('app_article.Article', on_delete=models.DO_NOTHING, related_name='offers')
    won = models.BooleanField()

    def __str__(self):
        return '%s %s, %s: %s' % (self.customer.user.first_name, self.customer.user.last_name, self.article.title, self.price)


class Inserat(models.Model):
    article = models.ForeignKey('app_article.Article', on_delete=models.DO_NOTHING)
    quantity = models.IntegerField()
    vendor = models.ForeignKey('app_user.Vendor', on_delete=models.DO_NOTHING)
    start_price = models.DecimalField(decimal_places=2, max_digits=6)
    buy_price = models.DecimalField(decimal_places=2, max_digits=6)
    increase_price = models.DecimalField(decimal_places=2, max_digits=6)
    current_price = models.DecimalField(decimal_places=2, max_digits=6)
    dispatch_type = models.ForeignKey('DispatchType', on_delete=models.DO_NOTHING)
    payment_type = models.ForeignKey('PaymentType', on_delete=models.DO_NOTHING)

    def __str__(self):
        return '%s x %s: %s' % (self.quantity, self.article.title, self.current_price)

    def save(self, *args, **kwargs):

        offers = Offer.objects.filter(article=self.article)
        totalprice = self.start_price
        for offer in offers:
            totalprice += offer.price

        self.current_price = totalprice
        super(Inserat, self).save(*args, **kwargs)
