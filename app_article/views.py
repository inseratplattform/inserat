# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from .models import Article, Category
from .serializers import ArticleSerializer, CategorySerializer


class ViewArticle(viewsets.ModelViewSet):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()


class ViewCategory(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
