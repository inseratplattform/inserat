from django.db import models


class Category (models.Model):
    name = models.CharField(max_length=100)
    icon = models.FileField(upload_to='uploads/icons/')
    sub_category = models.ForeignKey('Category', related_name='subcategorys', blank=True, null=True)

    def __str__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=100)
    category = models.ForeignKey('Category', on_delete=models.DO_NOTHING)
    used = models.BooleanField()
    description = models.CharField(max_length=1000)

    def __str__(self):
        return self.title


class Image(models.Model):
    imageSmall = models.FileField(upload_to='uploads/images/')
    imageLarge = models.FileField(upload_to='uploads/images/')
    article = models.ForeignKey('Article', on_delete=models.DO_NOTHING, related_name='images')

    def __str__(self):
        return self.article.title