from rest_framework import serializers

from .models import Article, Category, Image


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'icon', 'sub_category',)


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'imageSmall', 'imageLarge',)


class ArticleSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = Article
        fields = ('id', 'title', 'description', 'used', 'category', 'images',)


