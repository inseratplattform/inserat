
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from app_article.models import Category, Article, Image


admin.site.register(Category)
admin.site.register(Article)
admin.site.register(Image)

