from rest_framework.routers import DefaultRouter

from .views import ViewArticle, ViewCategory

router = DefaultRouter()
router.register(r'categories', ViewCategory, base_name='categories')
router.register(r'articles', ViewArticle, base_name='articles')
urlpatterns = router.urls