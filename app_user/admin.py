# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from app_user.models import Gender, Vendor, Customer

admin.site.register(Gender)
admin.site.register(Vendor)
admin.site.register(Customer)