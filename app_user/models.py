# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver


class Gender(models.Model):
    gender_code = models.CharField(max_length=20)
    gender = models.CharField(max_length=50, null=False)

    def __str__(self):
        return self.gender


class Profile(models.Model):
    gender = models.ForeignKey('Gender', on_delete=models.DO_NOTHING)
    birthdate = models.DateField()
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True

    def __str__(self):
        return ' %s %s %s' % (self.user.first_name, self.user.last_name, self.birthdate)


class Vendor(Profile):
    pass


class Customer(Profile):
    pass