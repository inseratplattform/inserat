from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Vendor, Customer, Gender


class GenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gender
        fields = ('id', 'gender_code', 'gender',)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name')


class VendorSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    gender = GenderSerializer(many=False)

    class Meta:
        model = Vendor
        fields = ('id', 'user', 'birthdate', 'gender',)


class CustomerSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    gender = GenderSerializer(many=False)

    class Meta:
        model = Customer
        fields = ('id', 'user', 'birthdate', 'gender',)