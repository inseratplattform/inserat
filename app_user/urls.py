from rest_framework.routers import DefaultRouter

from .views import ViewVendor, ViewCustomer

router = DefaultRouter()
router.register(r'vendors', ViewVendor, base_name='vendors')
router.register(r'customers', ViewCustomer, base_name='customers')
urlpatterns = router.urls